import re
import pandas as pd
from prefect import flow, task

import argparse



@task
def read_data(fn):
    with open(fn, 'r') as data_file:
        data = [line.strip() for line in data_file]
    return pd.DataFrame(data, columns=['sentences'])

@task
def features(data):
    data['words'] = data['sentences'].apply(lambda s: s.split()).apply(len)

@task
def predict(with_features):
    with_features['prediction'] = with_features['words']*3 + 1


@flow
def basic_prediction(fn):
    data = read_data(fn)
    features(data)
    predict(data)
    return data




def pars():
    p = argparse.ArgumentParser(description='test basic prediction flow')
    p.add_argument('textfile',
            type=str,
            action='store',
            help='name of text file containing lines to process')
    return p.parse_args()
    
def main():
    args = pars()
    basic_prediction(args.textfile)

if __name__ == '__main__':
    main()

import re
import pandas as pd
from prefect import flow, task

import argparse

avail_tasks = {}

def mytask(fn):
    t = task(fn)
    avail_tasks[fn.__name__] = t
    return t


def takes(*inputs):
    def inner(fn):
        fn.takes = tuple(inputs)
        return fn
    return inner

def returns(output):
    def inner(fn):
        fn.returns = output
        return fn
    return inner

@mytask
@takes('filename')
@returns('data')
def read_data(fn):
    with open(fn, 'r') as data_file:
        data = [line.strip() for line in data_file]
    return pd.DataFrame(data, columns=['sentences'])

@mytask
@takes('data')
@returns('with_features')
def features(data):
    data['words'] = data['sentences'].apply(lambda s: s.split()).apply(len)
    return data

@mytask
@takes('with_features')
@returns('with_predictions')
def predict(with_features):
    with_features['prediction'] = with_features['words']*3 + 1
    return with_features


@flow
def basic_prediction(fn):
    data = read_data(fn)
    with_features = features(data)
    with_predictions = predict(data)
    return with_predictions


def someone_takes():
    can_take = set()
    for task_name, task_fn in avail_tasks.items():
        can_take.update(task_fn.takes)
    return can_take

def someone_returns():
    returns = set()
    for task_name, task_fn in avail_tasks.items():
        returns.add(task_fn.returns)
    return returns

def breadth_first_forward(given, wanted):
    seq = breadth_first_forward_seq(given, wanted)
    print('found', seq)
    if seq is None:
        return None
    @flow
    def myflow():
        prev = []
        for res_state, task_fn in seq:
            result = task_fn(*prev)
            prev = [result]
    return myflow
            
def extend_one_node(latest_paths, functions, reachable,
        wanted=None):
    new_paths = {}
    for target, path in latest_paths.items():
        print(f'exploring from {target}')
        for task_name, task_fn in avail_tasks.items():
            print(f'trying {task_name}')
            ret = task_fn.returns
            if task_fn.takes == (target,):
                print(f'task {task_name} applies')
            else:
                continue
            if ret in reachable:
                print(f'but result {ret} is already known')
                continue
            print('new result: ' + ret)
            new_path = path + ((ret, task_fn),)
            new_paths[ret] = new_path
            reachable.add(ret)
            if wanted and ret == wanted:
                print(f'found wanted {wanted}')
                return {'new_paths': new_paths,
                        'returning': ret,
                        'found': new_path}
        print(f'tried all tasks in round')
    print(f'no more starting paths in round')
    return {'new_paths': new_paths}

def breadth_first_forward_seq(given, wanted):
    """
    given given function and wanted final state
    find sequence of tasks which will build a pipeline from 
    given.returns to wanted
    """
    starting_paths = {}
    starting_paths[given.returns] = (('init', given),)
    reachable = set()
    new_paths = starting_paths
    while new_paths and wanted not in reachable:
        step = extend_one_node(new_paths, avail_tasks, reachable, wanted=wanted)
        if 'found' in step:
            return step['found']
        new_paths = step['new_paths']
        if not new_paths:
            print(f'{wanted} not found but no more states accessible')
            break
    return None


def give_fn(fn):
    @returns('filename')
    def filename():
        return fn
    return filename



def get_predictions(fn):
    return test(fn)

def found_flow(fn, final_state='with_predictions'):
    sh = give_fn(fn)
    print(f'given returns {sh.returns}')
    for task_name, task_fn in avail_tasks.items():
        print(f'{task_name} takes {task_fn.takes} and returns {task_fn.returns}')
    found = breadth_first_forward(sh, final_state)
    return found





def pars():
    p = argparse.ArgumentParser(description='test building pipeline by breadth first search')

    p.add_argument('textfile',
            type=str,
            action='store',
            help='name of text file containing lines to process')
    p.add_argument('-f', '--final-state',
            type=str,
            action='store',
            choices=sorted(someone_returns()),
            default='with_predictions',
            help='desired final state name')
    return p.parse_args()
    
def main():
    args = pars()
    print('starting...')
    found = found_flow(args.textfile, args.final_state)
    if found is None:
        print('failed to find flow')
        return
    final = found()
    print(final)




if __name__ == '__main__':
    main()

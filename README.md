# self-assembling-dags

Summary: python module to demonstrate and support testable prefect-based pipelines and DAGs

s-a-d demonstrates a way to construct robust, testable, self-assembled data flows based on 
[prefect](https://www.prefect.io/) is a python framework for building data flows.

Out-of-the-box, prefect supports robust pipelines and other data flows with featues such as configurable retries and cached results.

The prefect documentation gives several relatively simple data flows which are hard-coded in python.  However, prefect also
supports data flows which are created dynamically; the goal of s-a-d is to demonstrate how to build such data flows in prefect.

Our emphasis is on several features:

- enabling realistic testing in the production environment by taking a production flow and
  - letting you specify arbitrary entry and exit points, without altering or forking
    the code which configures and manages the flow 
  - enabling repeated testing and debugging of downstream tasks 
    without having to redo all prior calculations
- specifying input requirements and output of each task, but letting the code figure out how to connect the pieces

